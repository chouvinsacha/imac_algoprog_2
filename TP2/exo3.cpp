#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
	void bubbleSort(Array& toSort)
	{
	
	int size = toSort.size();

    for (int i = 0; i < size - 1; i++) //On évite le out of range que
	{								   // pourrait provoquer le j+1
           for (int j = 0; j < size - i - 1; j++)
		   {
               if (toSort[j+1]<toSort[j]) //Si la valeur en dessous est
			   {						  //plus faible, elle remonte
                   toSort.swap(j, j+1);
               }
           }
    }

}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
