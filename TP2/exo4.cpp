#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

	// split
	if (size > 1)
	{
        int pivot = toSort[0];
        for (int i = 1 ;i < size; i++) 
		{
            if (toSort[i] > pivot)
			{
                greaterArray.insert(greaterSize, toSort[i]);
                greaterSize = greaterSize + 1;
            }

			else 
			{
                lowerArray.insert(lowerSize, toSort[i]);
                lowerSize = lowerSize + 1;
            }
        }

	// recursiv sort of lowerArray and greaterArray
	recursivQuickSort(Array& lowerArray, lowerSize) //On remplace les paramètres avec 
	recursivQuickSort(Array& greaterArray, int size)//les données des listes concernées

	// merge
	for(i = 0; i < lowerSize; i++)
	{
		toSort[i] = lowerArray[i];
	}

    toSort[lowerSize] = pivot;
    i = i + 1;
	
	for(i = lowerSize; i < greaterSize; i++)
	{
		toSort[i] = greaterArray[i-lowerSize];
	}
	/* Je n'ai pas fait le "t[i] ⇐ min(t1, t2)" parce-que je ne savais
	pas comment j'aurai pu gérer les deux indisces simultanéments pour
	pouvoir comparer le min du premier element de chaque liste, puis
	le deuxieme de lowerArray en gardant le premier de greaterArray etc...
	Du coup je ne fais que concaténer les deux listes. */
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
