#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value=value;
        this->left=NULL;
        this->right=NULL;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child

        Node* NewNode = createNode(value);

        if (value < this->value)
        {

            if (this->left != NULL)
            {
                this->left->insertNumber(value);
            }

            else
            {
                this->left = NewNode;
            }
        }

        if (value > this->value)
        {
            if (this->right != NULL)
            {
                this->right->insertNumber(value);
            }

            else
            {
                this->right = NewNode;
            }
        }


    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int Lheight = 0;
        int Rheight = 0;

        if (this->left != NULL)
            Lheight = this->left->height();

        if (this->right != NULL)
            Rheight = this->right->height();

        if(Lheight > Rheight)
        {
            return Lheight+1;
        }

        else
        {
            return Rheight+1;
        }

        return 1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int count = 1; // Compteur

        if (this->left != NULL)
            count = count + this->left->nodesCount();

        if (this->right != NULL)
            count = count + this->right->nodesCount();

            return count;
	}
        return 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        if (this->left == NULL and this->right == NULL)
        {
            return true;
        }

        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if (this->isLeaf() == true)
        {
            leaves[leavesCount] = this;
            leavesCount = leavesCount + 1;
        }

        if (this->left != NULL)
            this->left->allLeaves(leaves, leavesCount);

        if (this->right != NULL)
            this->right->allLeaves(leaves, leavesCount);
	}

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel

                                                            //Fils gauche
        if (this->left != NULL)                             
        {
            this->left->inorderTravel(nodes, nodesCount);
        }

                                                            //Parent
        nodes[nodesCount] = this;
        nodesCount = nodesCount + 1;

                                                            //Fils droit
        if (this->right != NULL)                            
        {
            this->right->inorderTravel(nodes, nodesCount);
        }


	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

                                                            //Parent
        nodes[nodesCount] = this;                           
        nodesCount = nodesCount + 1;

                                                            //Fils gauche
        if (this->left != NULL)                             
        {
            this->left->preorderTravel(nodes, nodesCount);
        }

                                                            //Fils droit
        if (this->right != NULL)                            
        {
            this->right->preorderTravel(nodes, nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

                                                            //Fils gauche
        if (this->left != NULL)                             
        {
            this->left->postorderTravel(nodes, nodesCount);
        }

                                                            //Fils droit
        if (this->right != NULL)                            
        {
            this->right->postorderTravel(nodes, nodesCount);
        }

                                                            //Parent
        nodes[nodesCount] = this;                           
        nodesCount = nodesCount + 1;
	}

	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
