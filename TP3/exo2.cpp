#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    indexMin = indexMax = -1;

	int start = 0;
    int end = array.size();

    while(start<end)
	{
        int mid = (start + end)/2;

        if(toSearch > array[mid])
		{
            start = mid + 1;
        }

        else if(toSearch < array[mid])
		{
            end = mid;
        }

        else 
		{
            indexMin = indexMax = mid;

            while(indexMin != 0 and array[indexMin - 1] == toSearch)
			{
                indexMin = indexMin - 1;
            }

            while(indexMax != array.size() - 1 and array[indexMax + 1] == toSearch)
			{
                indexMax = indexMax + 1;
            }

            break;
        }
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
