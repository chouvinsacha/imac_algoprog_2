#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
	return nodeIndex*2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
	return nodeIndex*2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
	this->get(i) = value;
	
	if (i%2 == 1)
	{
		while(i > 0 and this->get(i) > this->get((i-1)/2))
		{
			this->swap(i, (i-1)/2);
			i = (i-1)/2;
		}
	}

	if (i%2 == 0)
	{
		while(i > 0 and this->get(i) > this->get((i-2)/2))
		{
			this->swap(i, (i-2)/2);
			i = (i-2)/2;
		}
	}
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
	int i = 0;
	if (this->get(i_max) != i)
	{
		swap(i,i_max);
		i_max = i;
		this->heapify(heapSize, i_max);
	}
}

void Heap::buildHeap(Array& numbers)
{
	for (int i = 0; i < numbers.size(); i++)
	{
		insertHeapNode(numbers.size(), numbers[i])
	}

	i = 0;
	while (i_max = 0; i_max < 0; i_max--)
	{
		if (numbers[i]> numbers[0])
		{
			heapify(numnbers.size(), i_max);
		}
	}

}

void Heap::heapSort()
{

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
