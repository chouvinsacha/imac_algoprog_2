#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if(n > 0){

            if(z.length()>2)
            {
                return n;
            }

            z.x = pow(z.x,2) - pow(z.y,2) + point.x; 
            z.y = 2*z.x*z.y + point.y;

            float module = sqrt(pow(z.x,2)+pow(z.y,2));

            if(module > 2)
            {
                return n;
            }

            else
            {
                return isMandelbrot(z,n-1,point);
            }
        }

    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



